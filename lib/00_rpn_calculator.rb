class RPNCalculator

  def initialize
    @stack = []
    @value = 0
  end

  def push(num)
    @stack.push(num)
  end

  def value
    @stack[-1]
  end

  def plus
    raise "calculator is empty" if @stack.length < 2
    num1 = @stack.pop
    num2 = @stack.pop
    @value = num1 + num2
    @stack.push(@value)
    @value
  end

  def minus
    raise "calculator is empty" if @stack.length < 2
    num1 = @stack.pop
    num2 = @stack.pop
    @value = num2 - num1
    @stack.push(@value)
    @value
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    num1 = @stack.pop
    num2 = @stack.pop
    @value = num1 * num2
    @stack.push(@value)
    @value
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    num1 = @stack.pop
    num2 = @stack.pop
    @value = num2.to_f / num1
    @stack.push(@value)
    @value
  end

  def tokens(string)
    entry = string.split
    entry.map! do |el|
      if el.to_i.to_s == el
        el.to_i
      elsif el.to_f.to_s == el
        el.to_f
      else
        el.to_sym
      end
    end
  end

  def evaluate(string)
    token = tokens(string)
    while !token.empty?
      next_val = token.shift

      @stack.push(next_val) if next_val.is_a? Numeric
      self.plus if next_val == :+
      self.minus if next_val == :-
      self.times if next_val == :*
      self.divide if next_val == :/
    end

    @value
  end
end
